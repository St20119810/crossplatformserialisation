
#include "InputMemoryStream.h"
#include "InputMemoryBitStream.h"

#include "OutputMemoryStream.h"
#include "OutputMemoryBitStream.h"
#include "Bob.h"
#include <iostream>

using namespace std;

/* Glenn's main set up
#include <Windows.h>
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
*/

int main(void) {
	

	OutputMemoryStream *outStream = new OutputMemoryStream(); //creating an output stream 

	Bob bob(3.0f,2.0f,1.0f,3,2,1); //creating bob
	//Bob bob = Bob(); //This returns bob at 0 showing he is default
	//Send that packet

	bob.Write(*outStream); //serialisation of bob to the output stream
						

	//Pretend Bob is now Dead

	//Receive packet
	char* inBuffer = new char[256];
	int inBufferLength = outStream->GetLength();

	//this acts as sending the data out over the internet - in actual fact we're just storing it in a buffer
	memcpy(inBuffer, outStream->GetBufferPtr(), inBufferLength);

	//get the update that OG bob is dead
	InputMemoryStream *inStream = new InputMemoryStream(inBuffer, inBufferLength); //creating an input stream


	//This is new Bob (On another PC)
	Bob actorBob = Bob();

	//read that bob is dead and find out he is dead
	actorBob.Read(*inStream);
	
	cout << "If this is 3, Bob is dead: " << actorBob.getPosX() << endl; 
	
	/*Glenn's code
	int copyLen = outStream->GetLength();
	char* copyBuff = new char[copyLen];*/
	// Copy over the buffer
	//memcpy(inBuffer, outStream->GetBufferPtr(), copyLen);
	// Create a new memory stream
	/*
	inStream = new InputMemoryStream(copyBuff, copyLen);
	*/
	return 0;
}
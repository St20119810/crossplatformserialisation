#pragma once
#include "InputMemoryStream.h"
#include "OutputMemoryStream.h"

#include <cmath>
class Bob
{
private:
	float i, j, k;
	float bulletSpeed;
	int x, y, z;

public:
	Bob();
	Bob(float i, float j, float k, int x, int y, int z);
	~Bob();

	//inline setPos(int x, int y, int z); //Glenn said he expected the use of inline not int, why? because of the setup being in the main it no longer is so inline is not applicable

	/*void setPos(int x, int y, int z);
	int getPos(int x, int y, int z);
	In c++ it is not possible to return 3 values so it is necessary have a get for each value
	the set ups should also be separate in case user doesn't want to set all the positions axis' at once
	*/

	void setPosX(int x);
	void setPosY(int y);
	void setPosZ(int z);

	int getPosX();
	int getPosY();
	int getPosZ();
	float getBulletSpeed();

	void Write(OutputMemoryStream& out);
	void Read(InputMemoryStream& in);
};
